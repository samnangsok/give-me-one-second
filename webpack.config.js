const path = require('path');
const webpack = require('webpack');
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;
const PROXY = 'http://${HOST}:${PORT}';
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var ExtraTextPlugin = require('extract-text-webpack-plugin');

var extractPlugin = new ExtraTextPlugin({
  filename: 'master.css'
});

module.exports = {
  mode: 'development',
  entry: './src/js/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'build.js',
    publicPath: '/dist'
  },

  watch: true,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }
      },
      {
        test: /\.scss$/,
        use: extractPlugin.extract({
          use: ['css-loader', 'sass-loader']
        })
      }
    ]
  },
  plugins: [
    extractPlugin,
    new BrowserSyncPlugin(
      {
        host: HOST,
        port: PORT,
        files: ['./*.html'],
        server: {baseDir: ['./']}
      },
      {
        reload: true
      }
    )
  ]
};
