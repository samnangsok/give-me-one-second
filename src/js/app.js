import '../css/master.scss';
var secretButton = document.querySelector('#secret-button');
var secretPara = document.querySelector('#secret-p');

var showSecret = false;
secretButton.addEventListener('click', toggleSecretState);
updateSecretP();

function toggleSecretState(){
  showSecret = !showSecret;
  updateSecretP();
  updateSecretB()
}

function updateSecretB(){
  if (showSecret){
    secretButton.textContent = 'Hide the Secret';
  }else{
    secretButton.textContent = 'Show the Secret';
  }
}

function updateSecretP(){
  if (showSecret){
    secretPara.style.display = 'block';
  }else{
    secretPara.style.display = 'none';
  }
}
